#!/bin/bash

image=$1
name=$2
if (docker ps | grep $name)
then
echo 'exists and started'
docker exec -it --user="$(id -u):$(id -g $(whoami))" $name bash
elif (docker ps -a | grep $name)
then
echo 'exists but not started'
docker start $name
docker exec -it --user="$(id -u):$(id -g $(whoami))" $name bash
else
echo 'doesn`t exist'
docker run -d -v /home/developer/Documents/test:/tmp/ -v /etc/passwd:/etc/passwd:ro --name="$name" $image bash -c "while true; do sleep 60; done"
docker exec -it --user="$(id -u):$(id -g $(whoami))" $name bash
fi